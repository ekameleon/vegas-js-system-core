"use strict" ;

import Enum        from './src/Enum'
import Equatable   from './src/Equatable'
import Evaluable   from './src/Evaluable'
import Formattable from './src/Formattable'

import isEvaluable   from './src/isEvaluable'
import isFormattable from './src/Formattable'

/**
 * The system-core library is the root library for the <strong>VEGAS JS</strong> framework.
 * @summary The {@link system} library is the root package for the <strong>VEGAS JS</strong> framework.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system
 * @version 1.0.0
 * @since 1.0.0
 */
export default
{
    // interfaces

    Enum ,
    Equatable ,
    Evaluable ,
    Formattable ,

    // functions

    isEvaluable ,
    isFormattable
} ;