# VEGAS JS CORE OpenSource library - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.7] - 2021-05-14
### Changed
- Update the vegas-js-core dependency : 1.0.32
- Refactoring the sources
- Update the dev-dependencies in the package.json file.

## [1.0.6] - 2020-03-17
### Changed
- Fix the vegas-js-core version 1.0.19

## [1.0.4] - 2018-11-03
### Changed
- Fix the vegas-js-core version 1.0.5

## [1.0.0] - 2018-10-28
### Added
- First version

