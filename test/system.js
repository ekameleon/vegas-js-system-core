'use strict' ;

import chai from 'chai' ;

import Equatable   from '../src/Equatable'
import Enum        from '../src/Enum'
import Formattable from '../src/Formattable'

import isEquatable from '../src/isEquatable'
import isEvaluable from '../src/isEvaluable'
import isFormattable from '../src/isFormattable'

const assert = chai.assert ;

describe( 'system' , () =>
{
    it('Enum is not null', () => { assert.isNotNull( Enum ); });
    it('Equatable is not null', () => { assert.isNotNull( Equatable ); });
    it('Evaluable is not null', () => { assert.isNotNull( Enum ); });
    it('Formattable is not null', () => { assert.isNotNull( Formattable ); });
    
    it('isEvaluable is not null', () => { assert.isNotNull( isEquatable ); });
    it('isEvaluable is not null', () => { assert.isNotNull( isEvaluable ); });
    it('isFormattable is not null', () => { assert.isNotNull( isFormattable ); });
});
