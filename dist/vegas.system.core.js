/**
 * A specialized set of functions utilities that are highly reusable without creating any dependencies : arrays, strings, chars, objects, numbers, maths, date, colors, etc. - version: 1.0.6 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global.vegas_system_core = factory());
}(this, (function () { 'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  function Enum(value, name) {
    Object.defineProperties(this, {
      _name: {
        value: typeof name === "string" || name instanceof String ? name : "",
        enumerable: false,
        writable: true,
        configurable: true
      },
      _value: {
        value: isNaN(value) ? 0 : value,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  }
  Enum.prototype = Object.create(Object.prototype);
  Enum.prototype.constructor = Enum;
  Enum.prototype.equals = function (object) {
    if (object === this) {
      return true;
    }
    if (object instanceof Enum) {
      return object.toString() === this.toString() && object.valueOf() === this.valueOf();
    }
    return false;
  };
  Enum.prototype.toString = function () {
    return this._name;
  };
  Enum.prototype.valueOf = function () {
    return this._value;
  };

  function Equatable() {}
  Equatable.prototype = Object.create(Object.prototype);
  Equatable.prototype.constructor = Equatable;
  Equatable.prototype.equals = function (object) {
  };

  var Evaluable =
  function () {
    function Evaluable() {
      _classCallCheck(this, Evaluable);
    }
    _createClass(Evaluable, [{
      key: "eval",
      value: function _eval(value) {}
    }, {
      key: "toString",
      value: function toString() {
        return '[' + this.constructor.name + ']';
      }
    }]);
    return Evaluable;
  }();

  function Formattable() {}
  Formattable.prototype = Object.create(Object.prototype);
  Formattable.prototype.constructor = Formattable;
  Formattable.prototype.format = function (value) {
  };

  function isEvaluable(target) {
    if (target) {
      return target instanceof Evaluable || 'eval' in target && target.eval instanceof Function;
    }
    return false;
  }

  /**
   * The system-core library is the root library for the <strong>VEGAS JS</strong> framework.
   * @summary The {@link system} library is the root package for the <strong>VEGAS JS</strong> framework.
   * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
   * @author Marc Alcaraz <ekameleon@gmail.com>
   * @namespace system
   * @version 1.0.0
   * @since 1.0.0
   */
  var props = {
    Enum: Enum,
    Equatable: Equatable,
    Evaluable: Evaluable,
    Formattable: Formattable,
    isEvaluable: isEvaluable,
    isFormattable: Formattable
  };

  var skip = false ;
  function sayHello( name = '' , version = '' , link = '' )
  {
      if( skip )
      {
          return ;
      }
      try
      {
          if ( navigator && navigator.userAgent && navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
          {
              const args = [
                  `\n %c %c %c ${name} ${version} %c %c ${link} %c %c\n\n`,
                  'background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #000000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'background: #ff0000; padding:5px 0;',
              ];
              window.console.log.apply( console , args );
          }
          else if (window.console)
          {
              window.console.log(`${name} ${version} - ${link}`);
          }
      }
      catch( error )
      {
      }
  }
  function skipHello()
  {
      skip = true ;
  }

  var metas = Object.defineProperties({}, {
    name: {
      enumerable: true,
      value: 'vegas-js-system-core'
    },
    description: {
      enumerable: true,
      value: 'A specialized set of functions utilities that are highly reusable without creating any dependencies : arrays, strings, chars, objects, numbers, maths, date, colors, etc.'
    },
    version: {
      enumerable: true,
      value: '1.0.6'
    },
    license: {
      enumerable: true,
      value: "MPL-2.0 OR GPL-2.0+ OR LGPL-2.1+"
    },
    url: {
      enumerable: true,
      value: 'https://bitbucket.org/ekameleon/vegas-js-data'
    }
  });
  var bundle = _objectSpread({
    metas: metas,
    sayHello: sayHello,
    skipHello: skipHello
  }, props);
  try {
    if (window) {
      var load = function load() {
        window.removeEventListener("load", load, false);
        sayHello(metas.name, metas.version, metas.url);
      };
      window.addEventListener('load', load, false);
    }
  } catch (ignored) {}

  return bundle;

})));
//# sourceMappingURL=vegas.system.core.js.map
