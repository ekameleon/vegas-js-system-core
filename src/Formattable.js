/*jshint unused: false*/
"use strict" ;

/**
 * Interface implemented by classes that can format a value in a specific string expression.
 * @name Formattable
 * @memberof system
 * @interface
 */
export default function Formattable(){}

Formattable.prototype = Object.create( Object.prototype );
Formattable.prototype.constructor = Formattable;

/**
 * Formats the specified value.
 * @param {*} value - The object to evaluates.
 * @return the string representation of the formatted value.
 * @name eval
 * @memberof system.Formattable
 * @function
 * @instance
 */
Formattable.prototype.format = function( value ) 
{
    //
}