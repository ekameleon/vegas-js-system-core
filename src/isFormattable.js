/*jshint unused: false*/
"use strict" ;

import Formattable from './Formattable'

/**
 * Indicates if the specific objet is Formattable.
 * @name isFormattable
 * @function
 * @memberof system
 * @param {object} target - The object to evaluate.
 * @return {boolean} <code>true</code> if the object is {@link system.Formattable|Formattable}.
 */
const isFormattable = target => (target instanceof Formattable) || ( target && target.hasOwnProperty('format') && ( target.format instanceof Function ))

export default isFormattable ;