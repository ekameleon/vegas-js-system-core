/*jshint unused: false*/
"use strict" ;

import Evaluable from './Evaluable'

/**
 * Indicates if the specific objet is {@link system.Evaluable|Evaluable}.
 * @name isEvaluable
 * @function
 * @memberof system
 * @param {object} target - The object to evaluate.
 * @return {boolean} <code>true</code> if the object is {@link system.Evaluable|Evaluable}.
 */
const isEvaluable = target => target instanceof Evaluable || ( target && target.hasOwnProperty('eval') && ( target.eval instanceof Function ) )  ;

export default isEvaluable ;
