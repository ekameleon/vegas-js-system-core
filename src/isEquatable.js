/*jshint unused: false*/
"use strict" ;

import Equatable from './Equatable'

/**
 * Indicates if the specific objet is Equatable.
 * @function
 * @memberof system
 * @param {object} target - The object to evaluate.
 * @return {boolean} <code>true</code> if the object is {@link system.Equatable|Equatable}.
 */
const isEquatable = target => target instanceof Equatable || ( target && ( target.equals instanceof Function ) ) ;

export default isEquatable ;